# DocOps Registry

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

This project is to provide a set of requirements for documentation system starter templates, that would be compatible with the overall vision of The Good Docs Project.

Primary goals of this project:

* Help system template developers create more effective tools, by providing a comprehensive set of system requirements.
* Provide example content based on The Good Docs Project documentation Templates.
* Maintain a living registry of compatible system template projects.
* Reduce the time required to start a new project, or modify an existing project to take advantage of The Good Docs Project Templates.

Recent posts about choosing docs tooling, that this project hopes to help with.

* ["Choosing a docs tool" - Daryl White](https://djw.fyi/posts/choosing-a-docs-tool/)
* ["Software Documentation Tooling Decision Guide" - Brian Dominick](https://gist.github.com/briandominick/d4cbe11228de0ebe31cda630976af4ef)

## Scope

The DocOps Registry project is focused on a Docs-as-Code workflow, as this is in line with industry standards and limits the scope of the project to something manageable. For more information on Docs-as-Code, see:

* [Write the Docs: Docs as Code](https://www.writethedocs.org/guide/docs-as-code/)
* [I'd rather be writing: Docs as Code](https://idratherbewriting.com/trends/trends-to-follow-or-forget-docs-as-code.html)

For more information on DocOps, see:

* [Write the Docs: DocOps](https://www.writethedocs.org/guide/doc-ops/)

This project does not provide a working template system directly.
It will be up to developers of system templates, that conform to the standard, to submit their project to the registry for review and listing.

## Contributing

To submit a new project:

1. Make an issue in this project to add it into the registry.
2. Make a Pull/Merge Request that resolves the issue, with changes to the registry data file and the registry table.

After the Pull/Merge Request is reviewed and approved, it is up to a representative of the starter template project to maintain accurate information in the registry regarding that project.

Users of these system templates are encouraged to provide feedback and share experiences that they have with the starter templates here in this project.
Any issues/bugs or feature requests for a specific template should be made in that project's repository so that the developers can take direct action.

As this is an open source project itself, any comments, suggestions, constructive criticism, etc. are encouraged.
Please feel free to make a new issue with your feedback.

If you are interested in joining us to help define the specifications and review/approve new project submissions, please join the [DocOps Registry](https://www.thegooddocsproject.dev/working-groups#_docops_registry) working group.
