Title: `Convert rows {x-x}`

See Rows {x-x} in the Google Sheet: https://docs.google.com/spreadsheets/d/17nUuVEGf9uQE-SrpzY48BwdXliCQAqV4irncATxjric/edit#gid=2003069972

1) Navigate to the website of, and explore the capabilities of, each tool in this subset.

| Name                  |
|-----------------------|
| {toolname}            |

2) Answer the following questions about these tools:

* Is this tool open source or free to open source (so that we can validate the tool)? If you have any concerns over the future viability of this item, contact the team.
  * If "No", provide detail, contact DocOps leads to verify, do not proceed with database conversion for this item.
* Is this tool one that fits into a docs-as-code to output workflow? 
  * If "No", provide detail, contact DocOps leads to verify, do not proceed with database conversion for this item.
* Is this tool actively maintained (is there any activity within the codebase, or a release of the product, within the last 24 months)? If you have any concerns over the future viability of this item, contact the team.
  * If "No", provide detail, contact DocOps leads to verify, do not proceed with database conversion for this item.
* What are some of the primary features of this tool?

3) Convert existing data from `doctoolhub-tools.json` to the prototype data structure.

4) Categorise the tool as defined in the `component_definitions` document.

5) Add any relevant additional details relevant to the data structure.

6) Submit a MR with a file `<name>.yaml` in the `registry_content` folder, adding @bwklein, @barbaricyawps and @secondskoll as reviewers. 
Where the `name` is from the table above, with lowercase characters and `-` for spaces in the name.