# User Stories

This file documents the intended users of the Registry and the goals/stories that the system intends to address.
The Personas section identifies and details the types of individuals who might have need for the system.
The Stories section describes what goals and outcomes those Personas can expect from it.

## Personas

**Example Pattern:** `- Persona name - Persona description`

- Tool supplier - Contributor / developer of a tool that fills a role in a documentation workflow
- Project developer - [Doc System Owner Olly](https://docs.google.com/presentation/d/1P3zYifnt6vqJ_CYORu1vnQHduV_L94Ctydnssyh6bRI/edit#slide=id.p5)
- Technical writer - [Developer-Writer Darby](https://docs.google.com/presentation/d/1P3zYifnt6vqJ_CYORu1vnQHduV_L94Ctydnssyh6bRI/edit#slide=id.p7)
- Maintainer of the Doctools Registry - TGDP Doctools Registry team members

More potential TGDP persona stakeholders can be found at [tinyurl.com/tgdp-personas](https://tinyurl.com/tgdp-personas).

## Stories

**Example Pattern:** `- [Your_Initials] - As a [persona] I want to [achieve some goal] so we can [accomplish some outcome(s)].`

- [MP] - As a `project developer` I want to easily find what `options` are available to me for `developing and deploying documentation` for my product.
- [MP] - As a `project developer` I want to find a complete `chain of documentation tools`, compatible with my current `CI`, that can integrate `auto-generated API documentaion` from the codebase of my product.
- [MP] - As a `technical writer` I want to find `tools` that will `generate API documentation` from the codebase of a product.
- [MP] - As a `technical writer` I want to find `alternatives for tools` within an `existing chain of documentation tools` that currently generate and deploy documentation for a project.
- [SK] - As a `technical writer`, I want a `set of criteria` that will help me decide the most `suitable tool` from the `various options` available.
- [GB] - As a `technical writer` I want to use `criteria` based on tool `name`, `category`, `platform`, `output`, and `functionality` to find a `documentation tool` or generate a narrowed list of documentation tools `to create, manage, and publish` technical documentation.
- [GB] - As a `technical writer` I want to use `search` to find a documentation tool or generate a `narrowed list` of `documentation tools` to create, manage, and publish technical documentation.
- [GB] - As a `technical writer` I want to use `buttons and hyperlinks` to apply `filtering values` to find a documentation tool or generate a narrowed list of documentation tools to create, manage, and publish technical documentation.
- [GB] - As a `technical writer` I want to `view details about a selected tool` that includes a `brief functional description`, `outputs`, `compatible platforms`, `category`, `number of documentation toolchains` in which it is included, `screenshot`, and `website` of the tool to understand its suitability to my objectives.
- [GB] - As a `technical writer` I want to also view a `list of alternative tools` corresponding to my `search or filtering criteria`, alongside the `details of the selected tool`.
- [GB] - As a `technical writer` I want to `submit` a documentation tool or documentation toolchain for inclusion in the Doctools Registry.
- [GB] - As a `technical writer` I want to be able to `indicate my preference for a particular tool`, or otherwise `provide my feedback` on its `usage and features`, to advise the community on the `suitability` of the tool for a publishing scenario.
- [GB] - As a `maintainer of the Doctools Registry` I want to `curate submitted tools` to ensure tool `information is accurate and clear`, and `free of marketing language` that inflates or obscures tool descriptions, as well as to ensure the `proper categorization` of tools in the `Doctools Registry taxonomy`.
- [BK] - As a `technical writer` I want to find different tools for `public` vs. `internal` documentation.
- [MP] - As a `tool supplier` I want to `submit a new tool to the registry` relevant to a documentation deployment workflow by filling in a form.
- [MP] - As a `tool supplier` I want to `submit a change request for an existing tool` in the Doctools Registry by filling in a form.

## Information Requested

- Tool - Entry in the Registry, 'Options' are one or more 'Tools'.
- Prevalence (risky), preference, etc.
- Usage and Features
- Stack - Tool chain, collection of entries that provide everything from source to publishing.
- Experience 'posts', eventual writings about how specific tools/stacks work for people.  Similar to 'trip reports', 'travel blogs', 'etc.'  Justified and documented context.

## User Actions

- Search
- Filter
- Submit new entries
- Update entries
- Provide feedback
- View List
- View Detail

## Contributors

- [MP] - Michael Park
- [SK] - Sachin Karol
- [GB] - Greg Babb
- [BK] - Bryan Klein
