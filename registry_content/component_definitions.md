# Component definitions

> Note: This document is an early draft and will be updated as the work progresses.

Tools within the DocOps Registry are defined as one or more components within a proposed DocOps workflow. This document describes the components as established by the DocOps working group. These component definitions may be subject to change as this project evolves.

## Core components

Core components are required for the DocOps workflow to function. This may mean a single tool takes the role of all core components, or a series of tools are linked to provide this core functionality. Ideally, enhancement components can be used to provide additional functionality and value for the end documentation.

Core components have been categorised as follows:

* Content - The source files formats. It is assumed you have access to the source files to be able to process them.
* Transformer - Changes file formats from format to another. For example, Sphinx rST --> HTML.
* Assembler - Target formats organized into a coherent structure/relation.
* Distribution - How content gets to the end-user, such as on the web or in a PDF file. Alternative possible terms: consumption.
* Automations - Tools that minimize human involvement at any stage of the documentation process (from authoring to deployment). In this sense, the process through which you can apply the linked components together with a pre-set configuration. A lot of it is about minimizing human error and minimizing human interaction by ensuring you have a consistent environment or set of components each time.

## Enhancement components

Enhancement components are not required for the workflow to function. Enhancement components provide additonal value and may serve to provide functionality relating to best practices, or other critical areas, but if removed will not prevent the workflow from outputting documentation.

> NOTE: Enhancement components are more loosely defined, and will be expanded and or solidified as more tools are converted to the proposed data structure.

Enhancement components have been categorised as follows:

* Storage - Where files are stored, if that is a consideration. Assumption is Git (GitHub, GitLab, etc.)
* Content Editing Experience - Headless CMS, VSCode, Built-In Editing UI, etc.
* Testing/Linting:
  * In-Editor (SEO, Spelling, Grammar, Vale)
  * In CI/CD (Ekline, Vale)
* Analytics - Site visits, bounce rates, navigation flow/funnel, CTR
* Feedback - ratings, scores, comments, support requests, bugs/typos, missing information
* Site Search - Indexing content, storing the index, and providing a method to query the index.
* Search Engine Compatibility - sitemap.xml, crawlable links to all content, prerendered pages.
* Others?
