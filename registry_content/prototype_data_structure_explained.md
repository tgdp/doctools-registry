# Data structure

The DocOps Registry files use a YAML formatted data structure.
This is to increase the ease for human authoring and readability, while using a popular machine readable structured data format.

## Main structure

Each entry into the DocOps registry will contain the following properties:

- Name - Name of the tool
- Provider - Name of the author or organization
- URL - Location on the WWW
- Description - Short description of the registry entry
- Logo - URL or local path for logo display
- [Categories](#categorisation)
- Free
- Open Source
- License
- Link - How this tool is 'linked' into the toolchain.
- Extendable - Can the tool be extended with new features.
- Extensions (if extendable == true)
  - Extensions aim to provide additional properties that can be added to the main structure when the extension is enabled.
- Localization - Does it have native support for translations of input/output for the tool?
- Content Reuse - Does it support reusable input content, such as includes, snippets, templates, etc.?
- Transformer Features
- Assembler Features
- [Registry Properties](#registry-properties)

## Categorisation

The DocOps registry will categorise entries into one or more of the following key areas (with other categories to follow as applicable):

- Content - The source files formats.
NOTE: The location of the source files and where they are stored are part of the automations component.
- Transformer - Changes file formats from format to another. For example, Sphinx rST --> HTML.
- Assembler - Target formats organized into a coherent structure/relation.
- Distribution - How content gets to the end-user, such as on the web or in a PDF file.
Alternative possible terms: consumption.
- Automations - Tools that minimize human involvement at any stage of the documentation process (from authoring to deployment).
In this sense, the process through which you can apply the linked components together with a pre-set configuration.
A lot of it is about minimizing human error and minimizing human interaction by ensuring you have a consistent environment or set of components each time.

The data structure uses these categories to detail additional properties relating to each category.
For example, the prototype structure has the following properties due to the applied categories:

```yaml
transformer_features:
  autodoc: true
  autodoc_languages:
  - python
  - c
  - c++
  - javascript
assembler_features:
  site_search: true
```

These sections can be expanded as required.

## Registry properties

Another section has been added to compliment optional curation of the registry.
For example, the prototype data structure for Sphinx uses the following `registry_properties`:

```yaml
registry_properties:
  recommended: true
  tactic: false
  tactic_location: ''
```

This inidcates the entry is 'recommended', potentially showing that DocOps registry members have experience with the product and recommend it for use.
If a tactic article, or similar blog post has been created detailing use of the tool, then `tactic` and `tactic_location` can be used to detail and link to the blog entry.

## Subject to change

It was established by the DocOps registry group that this prototype structure would serve until it can be means tested and implemented.

Next steps will include establishing the base features of each category, any missing core data, and identify primary candidates for search functionality.
